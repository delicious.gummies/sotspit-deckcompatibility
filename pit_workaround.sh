#!/bin/bash
mkdir -p ~/.var/app/com.github.Matoking.protontricks/cache/winetricks/xna40
wget -O ~/.var/app/com.github.Matoking.protontricks/cache/winetricks/xna40/xnafx40_redist.msi https://web.archive.org/web/20131121002245if_/http://download.microsoft.com/download/A/C/2/AC2C903B-E6E8-42C2-9FD7-BEBAC362A930/xnafx40_redist.msi

flatpak run --branch=stable --arch=x86_64 --command=protontricks com.github.Matoking.protontricks 233700 dotnet40 xna40
